/*
 * gxr
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#ifndef GXR_H
#define GXR_H

#include <glib.h>

#define GXR_INSIDE

#include "gxr-version.h"
#include "gxr-io.h"
#include "gxr-math.h"
#include "gxr-time.h"
#include "openvr-action.h"
#include "openvr-action-set.h"
#include "openvr-compositor.h"
#include "openvr-context.h"
#include "openvr-overlay.h"
#include "openvr-system.h"

#undef GXR_INSIDE

#endif
